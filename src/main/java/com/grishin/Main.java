package com.grishin;

import com.grishin.config.Config;
import com.grishin.service.impl.IngredientServiceImpl;
import com.grishin.service.impl.RecipeServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        RecipeServiceImpl recipeService = context.getBean(RecipeServiceImpl.class);
        IngredientServiceImpl ingredientService = context.getBean(IngredientServiceImpl.class);

        //Ingredients
        System.out.println(ingredientService.getIngredient());
        System.out.println(ingredientService.findById(2));
        System.out.println(ingredientService.findByName("б"));

        //Recipes
        System.out.println(recipeService.getRecipes());
        System.out.println(recipeService.findById(2));

        context.close();
    }
}
