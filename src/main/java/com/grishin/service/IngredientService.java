package com.grishin.service;

import com.grishin.entity.Ingredient;

import java.util.List;

public interface IngredientService {
    List<Ingredient> getIngredient();

    Ingredient findById(int id);

    List<Ingredient> findByName(String name);

    void saveIngredient(Ingredient ingredient);

    void deleteById(int idToDelete);

    void updateById(int id, Ingredient updateIngredient);
}
