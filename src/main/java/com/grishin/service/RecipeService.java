package com.grishin.service;

import com.grishin.entity.Recipe;

import java.util.List;

public interface RecipeService {

    List<Recipe> getRecipes();

    Recipe findById(int id);

    List<Recipe> findByName(String name);

    void saveRecipe(Recipe recipe);

    void deleteById(int idToDelete);

    void updateById(int id, Recipe updateRecipe);
}
