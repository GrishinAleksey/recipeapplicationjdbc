package com.grishin.service.impl;

import com.grishin.entity.Ingredient;
import com.grishin.mapper.IngredientsMapper;
import com.grishin.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class IngredientServiceImpl implements IngredientService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public IngredientServiceImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Ingredient> getIngredient() {
        return jdbcTemplate.query("SELECT * FROM ingredients", new IngredientsMapper());
    }

    @Override
    public Ingredient findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM ingredients WHERE ing_id=?", new IngredientsMapper(), id);
    }

    @Override
    public List<Ingredient> findByName(String name) {
        String nameCount = "SELECT * FROM ingredients WHERE name LIKE ?";
        name = "%" + name + "%";
        return jdbcTemplate.query(nameCount, new IngredientsMapper(), name);
    }

    @Override
    public void saveIngredient(Ingredient ingredient) {
        jdbcTemplate.update("INSERT INTO ingredients VALUES(?, ?, ?)", ingredient.getId(), ingredient.getName());
    }

    @Override
    public void updateById(int id, Ingredient updateIngredient) {
        jdbcTemplate.update("UPDATE ingredients SET name=?, description=? WHERE ing_id=?", updateIngredient.getName(), id);
    }

    @Override
    public void deleteById(int idToDelete) {
        jdbcTemplate.update("DELETE FROM ingredients WHERE ing_id=?", idToDelete);
    }
}
