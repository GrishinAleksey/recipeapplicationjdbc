package com.grishin.service.impl;

import com.grishin.entity.Recipe;
import com.grishin.mapper.RecipeMapper;
import com.grishin.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RecipeServiceImpl implements RecipeService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RecipeServiceImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Recipe> getRecipes() {
        return jdbcTemplate.query("SELECT * FROM recipes", new RecipeMapper());
    }

    @Override
    public Recipe findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM recipes WHERE rec_id=?", new RecipeMapper(), id);
    }

    @Override
    public List<Recipe> findByName(String name) {
        String nameCount = "SELECT * FROM recipes WHERE name LIKE ?";
        name = "%" + name + "%";
        return jdbcTemplate.query(nameCount, new RecipeMapper(), name);
    }

    @Override
    public void saveRecipe(Recipe recipe) {
        jdbcTemplate.update("INSERT INTO recipes VALUES(?, ?, ?)", recipe.getId(), recipe.getName(), recipe.getDescription());
    }

    @Override
    public void updateById(int id, Recipe updateRecipe) {
        jdbcTemplate.update("UPDATE recipes SET name=?, description=? WHERE rec_id=?", updateRecipe.getName(), updateRecipe.getDescription(), id);
    }

    @Override
    public void deleteById(int idToDelete) {
        jdbcTemplate.update("DELETE FROM recipes WHERE rec_id=?", idToDelete);
    }
}
